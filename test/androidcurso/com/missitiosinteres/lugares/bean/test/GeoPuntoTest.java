package androidcurso.com.missitiosinteres.lugares.bean.test;

import junit.framework.Assert;

import org.junit.Test;

import androidcurso.com.missitiosinteres.lugares.bean.GeoPunto;

public class GeoPuntoTest {

	@Test
	public void testCalcularDistancia_DebeRetornarLaDistanciaDelPunto1111() {
		GeoPunto punto1 = new GeoPunto();
		punto1.setLatitud(1D);
		punto1.setLongitud(1D);
		GeoPunto punto2 = new GeoPunto();
		punto2.setLatitud(1D);
		punto2.setLongitud(1D);
		double experado = 0d;
		double distancia = punto1.calcularDistancia(punto2);
		Assert.assertEquals("Se esperaba que la distancia del punto P1(1,1) y P2(0,0) sea 0" + experado, experado, distancia);
	}

	@Test
	public void testCalcularDistancia_DebeRetornarLaDistanciaDelPunto1100() {
		GeoPunto punto1 = new GeoPunto();
		punto1.setLatitud(1D);
		punto1.setLongitud(1D);
		GeoPunto punto2 = new GeoPunto();
		punto2.setLatitud(0D);
		punto2.setLongitud(0D);
		double experado = Math.round(157422);
		double distancia = punto1.calcularDistancia(punto2);
		Assert.assertEquals("Se esperaba que la distancia del punto P1(1,1) y P2(0,0) sea " + experado, Math.round(experado), Math.round(distancia));
	}
	
	@Test
	public void testCalcularDistancia_DebeRetornarLaDistanciaDelPunto1110() {
		GeoPunto punto1 = new GeoPunto();
		punto1.setLatitud(1D);
		punto1.setLongitud(1D);
		GeoPunto punto2 = new GeoPunto();
		punto2.setLatitud(1D);
		punto2.setLongitud(0D);
		double experado = Math.round(111300);
		double distancia = punto1.calcularDistancia(punto2);
		Assert.assertEquals("Se esperaba que la distancia del punto P1(1,1) y P2(1,0) sea " + experado, Math.round(experado), Math.round(distancia));
	}
	
	@Test
	public void testCalcularDistancia_DebeRetornarLaDistanciaDelPunto1101() {
		GeoPunto punto1 = new GeoPunto();
		punto1.setLatitud(1D);
		punto1.setLongitud(1D);
		GeoPunto punto2 = new GeoPunto();
		punto2.setLatitud(0D);
		punto2.setLongitud(1D);
		double experado = Math.round(111317);
		double distancia = punto1.calcularDistancia(punto2);
		Assert.assertEquals("Se esperaba que la distancia del punto P1(1,1) y P2(1,0) sea " + experado, Math.round(experado), Math.round(distancia));
	}
}
