package androidcurso.com.missitiosinteres.lugares.bean;

import androidcurso.com.missitiosinteres.R;

public enum TipoLugar {
	OTROS("Otros", R.drawable.ic_otros),
	RESTAURANTE("Restaurante", R.drawable.ic_restaurante),
	BAR("Bar", R.drawable.ic_bar),
	COPAS("Copas", R.drawable.ic_copas),
	ESPECTACULO("Espectáculo", R.drawable.ic_espectaculos),
	HOTEL("Hotel", R.drawable.ic_hotel),
	COMPRAS("Compras", R.drawable.ic_compras),
	EDUCACION("Educación", R.drawable.ic_educacion),
	DEPORTE("Deporte", R.drawable.ic_deporte),
	NATURALEZA("Naturaleza", R.drawable.ic_naturaleza),
	GASOLINERA("Gasolinera", R.drawable.ic_gasolinera);

	private final String texto; 
	private final int recurso;  

	TipoLugar(String texto, int recurso) {
		this.texto = texto;
		this.recurso = recurso;
	}

	public String getTexto() { return texto; }

	public int getRecurso() { return recurso; }

	public static String[] getNombres() {
		String[] resultado = new String[TipoLugar.values().length];
		for(TipoLugar tipo : TipoLugar.values()) {
			resultado[tipo.ordinal()] = tipo.texto;
		}
		return resultado;
	}
}
