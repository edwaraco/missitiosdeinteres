package androidcurso.com.missitiosinteres.lugares.bean;

public class GeoPunto {
	public final double RADIO_TIERRA = 6378000;
	private double latitud;
	private double longitud;

	public GeoPunto() {
	}

	public GeoPunto(double latitud, double longitud) {
		this.latitud = latitud;
		this.longitud = longitud;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	@Override
	public String toString() {
		StringBuilder retorno = new StringBuilder();
		retorno.append("Coordenada {");
		retorno.append("Latitud: ");
		retorno.append(latitud);
		retorno.append(", Longitud: ");
		retorno.append(longitud);
		retorno.append("}");

		return retorno.toString();
	}

	public double calcularDistancia(GeoPunto puntoRef) {
		double dLat = Math.toRadians(latitud - puntoRef.latitud);
		double dLon = Math.toRadians(longitud - puntoRef.longitud);
		double lat1 = Math.toRadians(puntoRef.latitud);
		double lat2 = Math.toRadians(latitud);
		double a = Math.pow(Math.sin(dLat / 2), 2)
				+ Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1)
				* Math.cos(lat2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return c * RADIO_TIERRA;
	}

}
