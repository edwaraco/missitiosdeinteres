/**
 * 
 */
package androidcurso.com.missitiosinteres.lugares.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author earayoc
 * 
 */
public class Lugares {

	protected static List<Lugar> vectorLugares = ejemploLugares();

	public Lugares() {
		vectorLugares = ejemploLugares();
	}

	public static Lugar getLugar(int id) {
		return vectorLugares.get(id);
	}

	public static void adicionar(Lugar lugar) {
		vectorLugares.add(lugar);
	}

	public static int nuevo() {
		Lugar lugar = new Lugar();
		vectorLugares.add(lugar);
		return vectorLugares.size() - 1;
	}

	public static void borrar(int id) {
		vectorLugares.remove(id);
	}

	public static int getCantidadLugares() {
		return vectorLugares.size();
	}

	private static List<Lugar> ejemploLugares() {
		ArrayList<Lugar> lugares = new ArrayList<Lugar>();
		lugares.add(new Lugar("Escuela Polit�cnica Superior de Gand�a",
				"C/ Paranimf, 1 46730 Gandia (SPAIN)", -0.166093, 38.995656,
				962849300, "http://www.epsg.upv.es",
				"Uno de los mejores lugares para formarse.", 3,
				TipoLugar.EDUCACION));

		lugares.add(new Lugar("Al de siempre",
				"P.Industrial Junto Mol� Nou - 46722, Benifla (Valencia)",
				-0.190642, 38.925857, 636472405, "",
				"No te pierdas el arroz en calabaza.", 3, TipoLugar.BAR));

		lugares.add(new Lugar("androidcurso.com", "ciberespacio", 0.0, 0.0,
				962849300, "http://androidcurso.com",
				"Amplia tus conocimientos sobre Android.", 5,
				TipoLugar.EDUCACION));

		lugares.add(new Lugar(
				"Barranco del Infierno",
				"V�a Verde del r�o Serpis. Villalonga (Valencia)",
				-0.295058,
				38.867180,
				0,
				"http://sosegaos.blogspot.com.es/2009/02/lorcha-villalonga-via-verde-del-rio.html",
				"Espectacular ruta para bici o andar", 4, TipoLugar.NATURALEZA));

		lugares.add(new Lugar("La Vital",
				"Avda. de La Vital, 0 46701 Gand�a (Valencia)", -0.1720092,
				38.9705949, 962881070, "http://www.lavital.es/",
				"El t�pico centro comercial", 2, TipoLugar.COMPRAS));

		return lugares;

	}

	public static List<Lugar> filtrarListaPorNombres(String nombre) {
		List<Lugar> listaLugares = new ArrayList<Lugar>();
		for (Lugar lugar : ejemploLugares()) {
			if (lugar.getNombre() != null && lugar.getNombre().contains(nombre)) {
				listaLugares.add(lugar);
			}
		}
		return listaLugares;
	}

	public static List<String> getArrayNombres(List<Lugar> lugares) {
		List<String> resultado = new ArrayList<String>();
		for (Lugar lugar : lugares) {
			resultado.add(lugar.getNombre());
		}
		return resultado;
	}
}