package androidcurso.com.missitiosinteres;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidcurso.com.missitiosinteres.activities.AcercaDe;
import androidcurso.com.missitiosinteres.activities.PreferenciaAndroidHoneyComb;
import androidcurso.com.missitiosinteres.activities.PreferenciaAndroidMenorApi8;
import androidcurso.com.missitiosinteres.activities.sitios.ListaLugares;
import androidcurso.com.missitiosinteres.activities.sitios.VistaLugar;

public class MainActivity extends Activity {

	private Button btnSalir;
	private Button btnPreferencias;
	private Button btnVistaLugar;
	private static final int ID_BUSQ_POR_POSICION_EN_ARREGLO_LUGAR = 1;
	private static final int ID_BUSQ_POR_NOMBRE_LUGAR = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mis_sitios_main);
		this.crearEventoBotonSalir();
		this.crearEventoBotonPreferencias();
		this.crearEventoBotonVistaLugar();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.mis_sitios_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_mostrar_acerca_de:
			irAcercaDe(null);
			break;
		case R.id.menu_mostrar_preferencias:
			irAPreferencias(null);
			break;
		}
		return true;
	}

	public void crearEventoBotonSalir() {
		btnSalir = (Button) findViewById(R.id.btn_salir);
		btnSalir.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				finalizarApp(view);
			}
		});
	}

	public void crearEventoBotonPreferencias() {
		btnPreferencias = (Button) findViewById(R.id.btn_mostrar_preferencias);
		btnPreferencias.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				irAPreferencias(view);
			}
		});
	}
	
	public void crearEventoBotonVistaLugar() {
		btnVistaLugar = (Button) findViewById(R.id.btn_mostrar_lugares);
		btnVistaLugar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				irMostrarLugar(view);
			}
		});
	}

	public void irAcercaDe(View view) {
		Intent intentAcercaDe = new Intent(this, AcercaDe.class);
		this.startActivity(intentAcercaDe);
	}
	
	private final LinearLayout crerViewParaBusquedaLugar() {
		LinearLayout linearLayout = new LinearLayout(this);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		EditText entradaIdLong = new EditText(this);
		entradaIdLong.setId(ID_BUSQ_POR_POSICION_EN_ARREGLO_LUGAR);
		EditText entradaNombre = new EditText(this);
		entradaNombre.setId(ID_BUSQ_POR_NOMBRE_LUGAR);
		TextView lblBusqPorIdLugar = new TextView(this.getBaseContext());
		TextView lblBusqPorNombreLugar = new TextView(this.getBaseContext());
		lblBusqPorIdLugar.setText(R.string.hint_solicitar_id_lugar);
		lblBusqPorNombreLugar.setText(R.string.hint_solicitar_nombre_lugar);
		entradaIdLong.setText("");
		entradaNombre.setText("");
		linearLayout.addView(lblBusqPorIdLugar);
		linearLayout.addView(entradaIdLong);
		linearLayout.addView(lblBusqPorNombreLugar);
		linearLayout.addView(entradaNombre);
		return linearLayout;
	}
	private void filtrarLugaresPorNombre(View view, String nombreFiltro) {
		Intent i = new Intent(MainActivity.this, ListaLugares.class);
        i.putExtra("nombre", nombreFiltro);
        startActivity(i);  
	}
	
	private void getVistaLugarSegunFiltro(View view) {
		EditText entradaIdLong = (EditText) view.findViewById(ID_BUSQ_POR_POSICION_EN_ARREGLO_LUGAR);
		EditText entradaNombre = (EditText) view.findViewById(ID_BUSQ_POR_NOMBRE_LUGAR);
		String posicion = entradaIdLong.getText().toString();
		String nombre = entradaNombre.getText().toString();
		if (!"".equals(posicion)) {
			long id = Long.parseLong(entradaIdLong.getText().toString());
			Intent i = new Intent(MainActivity.this, VistaLugar.class);
			i.putExtra("id", id);
			startActivity(i);    
		} else if (!"".equals(nombre)) {
			filtrarLugaresPorNombre(view, nombre);
		}
	}
	
	public void irMostrarLugar(View view) {
		final LinearLayout linearLayout = this.crerViewParaBusquedaLugar();
        new AlertDialog.Builder(this)
           .setTitle(R.string.title_busqueda_lugar)
           .setView(linearLayout)
           .setPositiveButton(R.string.accion_buscar, new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int whichButton) {
            	   getVistaLugarSegunFiltro(linearLayout);
               }})
           .setNegativeButton(R.string.button_cancel, null)
           .show();
	}

	public void irAPreferencias(View view) {
		Intent i = null;
		Log.i("MyActivity#irAPreferencias", "SDK_VERSION "
				+ Build.VERSION.SDK_INT);
		i = new Intent(this, PreferenciaAndroidMenorApi8.class);
		startActivity(i);
	}

	public void finalizarApp(View view) {
		finish();
	}
	
	
}
