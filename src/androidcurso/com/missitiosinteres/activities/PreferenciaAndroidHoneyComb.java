package androidcurso.com.missitiosinteres.activities;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import androidcurso.com.missitiosinteres.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PreferenciaAndroidHoneyComb extends PreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.addPreferencesFromResource(R.xml.preferences);
	}
}
