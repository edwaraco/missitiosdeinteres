package androidcurso.com.missitiosinteres.activities.sitios;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import androidcurso.com.missitiosinteres.R;
import androidcurso.com.missitiosinteres.lugares.bean.Lugar;
import androidcurso.com.missitiosinteres.lugares.bean.Lugares;
import androidcurso.com.missitiosinteres.lugares.bean.TipoLugar;

public class EdicionLugar extends Activity {

	private Lugar lugar;
	private EditText nombre;
	private Spinner tipo;
	private EditText direccion;
	private EditText telefono;
	private EditText url;
	private EditText comentario;

	private EditText buscarYAsignarTexto(int idTextView, String textoAsignar) {
		EditText editText = (EditText) findViewById(idTextView);
		editText.setText(textoAsignar);
		return editText;
	}

	private void asignarInformacionLugarAVistaLugar() {
		nombre = this.buscarYAsignarTexto(R.id.txt_nom_lugar,
				this.lugar.getNombre());
		direccion = this.buscarYAsignarTexto(R.id.txt_dir_lugar,
				this.lugar.getDireccion());
		telefono = this.buscarYAsignarTexto(R.id.txt_tel_lugar,
				String.valueOf(this.lugar.getTelefono()));
		url = this.buscarYAsignarTexto(R.id.txt_url, this.lugar.getUrl());
		comentario = this.buscarYAsignarTexto(R.id.txt_comentario,
				this.lugar.getComentario());
		tipo = (Spinner) findViewById(R.id.sel_tipo_lugar);
		ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, TipoLugar.getNombres());
		adaptador
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tipo.setAdapter(adaptador);
		tipo.setSelection(lugar.getTipoLugar().ordinal());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edicion_lugar);
		Bundle extras = getIntent().getExtras();
		long id = extras.getLong("id", 0);
		Log.i("EdicionLugar#onCreate", "Id " + id);
		this.lugar = Lugares.getLugar((int) id);
		Log.i("EdicionLugar#onCreate", "Lugar " + this.lugar);
		asignarInformacionLugarAVistaLugar();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.edicion_lugar, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.accion_guardar:
			guardarLugar();
			break;
		case R.id.accion_cancelar:
			finish();
			break;
		}
		return true;
	}

	private void guardarLugar() {
		lugar.setNombre(nombre.getText().toString());
		lugar.setTipoLugar(TipoLugar.values()[tipo.getSelectedItemPosition()]);
		lugar.setDireccion(direccion.getText().toString());
		lugar.setTelefono(Integer.parseInt(telefono.getText().toString()));
		lugar.setUrl(url.getText().toString());
		lugar.setComentario(comentario.getText().toString());
		finish();
	}
}