package androidcurso.com.missitiosinteres.activities.sitios;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import androidcurso.com.missitiosinteres.R;
import androidcurso.com.missitiosinteres.lugares.bean.Lugar;
import androidcurso.com.missitiosinteres.lugares.bean.Lugares;

public class ListaLugares extends ListActivity {
	public BaseAdapter adaptador;

	@SuppressLint("NewApi")
	private void getArrayAdapterPorNombre() {
		Bundle extras = getIntent().getExtras();
		String nombre = extras.getString("nombre", "");
		List<Lugar> lugares = Lugares.filtrarListaPorNombres(nombre);
		adaptador = new ArrayAdapter<Lugar>(this,	android.R.layout.simple_list_item_1, lugares);
		setListAdapter(adaptador);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_activity_lugares_filtrados);
		getArrayAdapterPorNombre();
	}
}
