package androidcurso.com.missitiosinteres.activities.sitios;

import java.text.DateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import androidcurso.com.missitiosinteres.R;
import androidcurso.com.missitiosinteres.lugares.bean.Lugar;
import androidcurso.com.missitiosinteres.lugares.bean.Lugares;

public class VistaLugar extends Activity {
	private long id;
	private Lugar lugar;

	private TextView buscarYAsignarTexto(int idTextView, String textoAsignar) {
		TextView textView = (TextView) findViewById(idTextView);
		textView.setText(textoAsignar);
		return textView;
	}

	private void asignarInformacionLugarAVistaLugar() {
		TextView nombre = this.buscarYAsignarTexto(R.id.nombre,
				this.lugar.getNombre());
		TextView tipo = this.buscarYAsignarTexto(R.id.tipo, this.lugar
				.getTipoLugar().getTexto());
		tipo.setCompoundDrawablesWithIntrinsicBounds(this.lugar.getTipoLugar()
				.getRecurso(), 0, 0, 0);
		TextView direccion = this.buscarYAsignarTexto(R.id.direccion,
				this.lugar.getDireccion());
		TextView telefono = this.buscarYAsignarTexto(R.id.telefono,
				String.valueOf(this.lugar.getTelefono()));
		TextView url = this.buscarYAsignarTexto(R.id.url, this.lugar.getUrl());
		TextView comentario = this.buscarYAsignarTexto(R.id.comentario,
				this.lugar.getComentario());
		TextView fecha = this.buscarYAsignarTexto(R.id.fecha, DateFormat
				.getDateInstance().format(new Date(this.lugar.getFecha())));
		TextView hora = this.buscarYAsignarTexto(R.id.hora, DateFormat
				.getTimeInstance().format(new Date(this.lugar.getFecha())));
		RatingBar valoracion = (RatingBar) findViewById(R.id.valoracion);
		valoracion.setRating(this.lugar.getValoracion());
		valoracion
				.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
					@Override
					public void onRatingChanged(RatingBar ratingBar,
							float valor, boolean fromUser) {
						lugar.setValoracion(valor);
					}
				});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vista_lugar);
		Bundle extras = getIntent().getExtras();
		id = extras.getLong("id", 0);
		this.lugar = Lugares.getLugar((int) id);
		asignarInformacionLugarAVistaLugar();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.vista_lugar, menu);
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {
			asignarInformacionLugarAVistaLugar();
			findViewById(R.id.scrollView1).invalidate();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.accion_compartir:
			break;
		case R.id.accion_llegar:
			break;
		case R.id.accion_editar:
			irAEdicionLugar();
			break;
		case R.id.accion_borrar:
			elimnarLugar(null);
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void irAEdicionLugar() {
		Intent i = new Intent(VistaLugar.this, EdicionLugar.class);
		i.putExtra("id", id);
		startActivityForResult(i, 1);
	}

	private void elimnarLugar(View view) {
		new AlertDialog.Builder(this)
		.setTitle(R.string.title_eliminar_lugar)
		.setMessage(R.string.hint_eliminar_lugar)
		.setPositiveButton(R.string.accion_borrar, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int whichButton) {
				Lugares.borrar((int) id);
				finish();
			}
		})
		.setNegativeButton(R.string.button_cancel, null).show();
	}
}