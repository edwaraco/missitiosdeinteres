package androidcurso.com.missitiosinteres.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import androidcurso.com.missitiosinteres.R;

public class PreferenciaAndroidMenorApi8 extends PreferenceActivity {

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.addPreferencesFromResource(R.xml.preferences);
	}
}
